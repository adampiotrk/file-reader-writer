import java.util.List;
import java.util.Map;

public class App {
    public static void main(String[] args) {
        String filePath = "C:\\Users\\adamk\\IdeaProjects\\CSVReader\\src\\main\\resources\\rules.csv";
        IFileReader fileReader = new CSVFileReader(filePath);
        try {
            List<Map<String,String>> fileContent = fileReader.getRules();
            System.out.println("Liczba wierszy w pliku: " + fileContent.size());
        } catch (MyFileReaderException e) {
            e.printStackTrace();
        }
    }
}
