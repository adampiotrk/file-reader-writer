public class MyFileReaderException extends Throwable {
    public MyFileReaderException(String message, Throwable cause) {
        super(message, cause);

    }
}
